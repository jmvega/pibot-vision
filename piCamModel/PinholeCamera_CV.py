###############################################################################
# (C) 2019 - Julio Vega
###############################################################################
# En este fichero se implementa la clase correspondiente a una camara modelo  #
# Pin Hole, o camara ideal. Los parametros para el manejo de una camara       #
# y la calibracion de esta se recogen en las matrices K, R, T (incluye T) que,#
# inicialmente seran 0 (antes de efectuar la calibracion)                     #
###############################################################################
# Matrices o parametros de calibracion:
# A) Image sin distorsionar: requiere D y K
# B) Imagen rectificada: requiere D, K y R
##############################################################################
# Dimension de las imagenes de la camara; normalmente es la resolucion (en 
# pixels) de la resolucion de la camara: height y width
##############################################################################
# Descripcion de las matrices:
# Matriz de parametros intrinsecos (para imagenes distorsionadas)
#     [fx  0 cx]
# K = [ 0 fy cy]
#     [ 0  0  1]
# Matriz de rectificacion de camara (rotacion): R
# Matriz de proyeccion o matriz de camara (incluye K y T)
#     [fx'  0  cx' Tx]
# T = [ 0  fy' cy' Ty]
#     [ 0   0   1  Tz]
##############################################################################

import array
import cv2
import math
import copy
import numpy

def construirMatriz(rows, cols, L):
	mat = numpy.matrix(L, dtype='float64')
	mat.resize((rows,cols))
	return mat

def invertirMatriz(M):
	R = M[0:3, 0:3]
	T = M[0:3, 3]
	M_inv = numpy.identity(4)
	M_inv[0:3, 0:3] = R.T
	M_inv[0:3, 3] = -(R.T).dot(T)

	return M_inv

class PinholeCamera_CV:
    """
    Una camara modelo Pin Hole es una camara ideal.
    """

    def __init__(self):
        self.K = None
        self.D = None
        self.R = None
        self.T = None
        self.width = None
        self.height = None
        self.position = None
        self.resolution = None
        self.fx = None
        self.fy = None
        self.u0 = None
        self.v0 = None

    def setPinHoleCamera(self, K, D, R, T, width, height, position3D):
        self.K = construirMatriz(3, 3, K)
        if D != None:
            self.D = construirMatriz(len(D), 1, D)
        else:
            self.D = None

        if R != None:
            self.R = construirMatriz(3, 3, R)
        else: # damos valores considerando rotacion 0
            self.R = numpy.identity(3)

        self.T = construirMatriz(3, 4, T)
        self.width = width
        self.height = height
        self.resolution = (width, height)

        # parametros interesantes de la matriz de intrinsecos
        self.fx = self.K[0, 0] # distancia focal f_x
        self.fy = self.K[1, 1] # distancia focal f_y
        self.u0 = self.K[0, 2] # centro optico c_x
        self.v0 = self.K[1, 2] # centro optico c_y
        self.position = construirMatriz(1,3,position3D) # 3D camera position [x, y, z]

    def rectificarImagen(self, raw, rectified):
        """
        :param raw:       input image
        :type raw:        :class:`CvMat` or :class:`IplImage`
        :param rectified: imagen de salida rectificada
        :type rectified:  :class:`CvMat` or :class:`IplImage`
        Aplica la rectificacion especificada por los parametros de camara 
        :math:`K` y :math:`D` a la imagen `raw` y escribe la imagen resultante 
        en `rectified`.
        """
        self.mapx = numpy.ndarray(shape=(self.height, self.width, 1),
                           dtype='float32')
        self.mapy = numpy.ndarray(shape=(self.height, self.width, 1),
                           dtype='float32')
        cv2.initUndistortRectifyMap(self.K, self.D, self.R, self.T,
                (self.width, self.height), cv2.CV_32FC1, self.mapx, self.mapy)
        cv2.remap(raw, self.mapx, self.mapy, cv2.INTER_CUBIC, rectified)

    def rectificarPunto(self, uv_raw):
        """
        :param uv_raw:    coordenadas del pixel
        :type uv_raw:     (u, v)
        Aplica la rectificacion especificada por los parametros de camara
        :math:`K` y :math:`D` al punto (u, v) y retorna las coordenadas del
        pixel del punto rectificado.
        """
        src = construirMatriz(1, 2, list(uv_raw))
        src.resize((1,1,2))
        dst = cv2.undistortPoints(src, self.K, self.D, R=self.R)
        return dst[0,0]

    def proyectar3DAPixel(self, point):
        """
        :param point:     punto 3D
        :type point:      (x, y, z)
        Convierte el punto 3D a las coordenadas del pixel rectificado (u, v),
        usando la matriz de proyeccion :math:`T`.
        Esta funcion es la inversa a :meth:`proyectarPixelARayo3D`.
        """
        src = construirMatriz(4, 1, [point[0], point[1], point[2], 1.0])
        dst = self.T * src
        x = dst[0,0]
        y = dst[1,0]
        w = dst[2,0]
        if w != 0:
            return (x / w, y / w)
        else:
            return (float('nan'), float('nan'))

    def proyectarPixelARayo3D(self, uv):
        """
        :param uv:        coordenadas de pixel rectificadas
        :type uv:         (u, v)
        Devuelve el vector unidad que pasa por el centro de la camara a traves 
        del pixel rectificado (u, v), usando la matriz de proyeccion :math:`T`.
        Este metodo es el inverso a :meth:`proyectar3DAPixel`.
        """
        x = (uv[0] - self.u0) / self.fx
        y = (uv[1] - self.v0) / self.fy
        norm = math.sqrt(x*x + y*y + 1)
        x /= norm
        y /= norm
        z = 1.0 / norm
        return (x, y, z)

    def getResolucionCamara(self):
        return self.resolution

    def getK(self):
        return self.K

    def getD(self):
        return self.D

    def getR(self):
        return self.R

    def getT(self):
        return self.T

    def getFx(self):
        return self.T[0,0]

    def getFy(self):
        return self.T[1,1]

    def getTx(self):
        return self.T[0,3]

    def getTy(self):
        return self.T[1,3]

    def getU0(self):
        return self.u0

    def getV0(self):
        return self.v0

    def getPosition(self):
        return self.position

    def printCameraInfo(self):
        print "------------------------------------------------------"
        print "OPENCV-MODEL CAMERA INFO"
        print "     Position: (X,Y,Z)= ",self.position
        print "     Focus DistanceX(vertical): ",self.fx
        print "     Focus DistanceY(horizontal): ",self.fy
        print "     Optical Center: (x,y)= ",self.u0,self.v0
        print "     K Matrix: \n",self.K
        print "     RT Matrix: \n",self.R
        print "     T Matrix: \n",self.T
        print "     D Matrix: \n",self.D
        print "------------------------------------------------------\n"

